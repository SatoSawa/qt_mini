==========================================================   
1.Extract Qt_mini.zip   
==========================================================   
These libs are only for building the simplest(Maybe?) QT Application.    
I create this because I don;t want the compilcation environment takes long time to set up (Complete Qt lib > 1GB, takes long time to copy)      



==========================================================   
2. Set the Environment Variable    
==========================================================   
Variable Name	= QTDIR  
Value		    = C:\Qt_mini\msvc2015_64   
   
and   
   
Variable Name   = PATH   
Value           = %QTDIR%\bin   
   
and   
   
Variable Name   = QT_QPA_PLATFORM_PLUGIN_PATH   
Value           = %QTDIR%\plugins\platforms\   
   
e.g.  if you put Qt_mini folder to C:\ then set C:\Qt_mini\msvc2015_64   
   
==========================================================   
3. Install Qt Plugin in Visual Studio   
==========================================================   
Click toolbar of Visual Studio :Tools->Extensions and Updates->QtPackage->Install   
   
   
==========================================================    
4. Set Qt Version in Visual Studio    
==========================================================   
Click  toolbar of Visual Studio: QT5->Add    
Version name	: 5.7mini    
Path		: C:\Qt_mini\msvc2015_64     //the path of "Qt_mini\msvc2015_64"   
  
   
==========================================================   
5.Finish   
==========================================================   
Now you can create QT project:    
Click  toolbar: File->Project->(In Visual C++)Qt5 Projects->Qt Application    
